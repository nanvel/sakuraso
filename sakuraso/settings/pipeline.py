PIPELINE_CSS = {
    'login': {
        'source_filenames': (
            'css/login.less',
        ),
        'output_filename': 'css/login.min.css',
    },
    'dashboard': {
        'source_filenames': (
            'css/dashboard.less',
        ),
        'output_filename': 'css/dashboard.min.css',
    },
    'streams': {
        'source_filenames': (
            'css/streams.less',
        ),
        'output_filename': 'css/streams.min.css',
    },
    'stream': {
        'source_filenames': (
            'css/stream.less',
        ),
        'output_filename': 'css/stream.min.css',
    }
}

PIPELINE_JS = {
    'base': {
        'source_filenames': (
            'js/libs/jquery-2.0.0.min.js',
            'js/core.js',
        ),
        'output_filename': 'js/libs/base.min.js',
    },
    'stream': {
        'source_filenames': (
            'js/stream.js',
        ),
        'output_filename': 'js/stream.min.js',
    },
}

# When PIPELINE is True, CSS and JavaScripts will be concatenated and filtered.
# When False, the source-files will be used instead.
# Default: PIPELINE = not DEBUG

PIPELINE_CSS_COMPRESSOR = 'pipeline.compressors.yui.YUICompressor'
PIPELINE_JS_COMPRESSOR = 'pipeline.compressors.yui.YUICompressor'

PIPELINE_YUI_BINARY = '/usr/bin/yui-compressor'

PIPELINE_COMPILERS = (
  'sakuraso.apps.core.compilers.RubyLesscCompiler',
)

PIPELINE_LESS_BINARY = '/usr/local/bin/lessc'

PIPELINE_DISABLE_WRAPPER = True
