from django.conf.urls import patterns, url


urlpatterns = patterns('sakuraso.apps.tasks.views',
    url(r'^$', 'index', name='tasks-dashboard'),
    url(r'^streams/$', 'streams', name='tasks-streams'),
    url(r'^streams/(?P<stream_id>\d+)/$', 'stream', name='tasks-stream'),
    url(r'^streams/status/$', 'stream_status_change', name='tasks-stream-status-change'),
)
