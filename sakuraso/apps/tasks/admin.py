from django.contrib import admin

from .models import Stream, Task, TaskContent


class StreamAdmin(admin.ModelAdmin):
    list_display= ('__unicode__',)
    search_fields = ('title',)


class TaskAdmin(admin.ModelAdmin):
    list_display= ('__unicode__', 'stream', 'timestamp', 'priority')
    list_filter = ('priority',)
    search_fields = ('title',)


class TaskContentAdmin(admin.ModelAdmin):
    list_display= ('__unicode__', 'task', 'timestamp')


admin.site.register(Stream, StreamAdmin)
admin.site.register(Task, TaskAdmin)
admin.site.register(TaskContent, TaskContentAdmin)
