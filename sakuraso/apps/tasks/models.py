import datetime

from django.db import models


class Stream(models.Model):

    ACTIVE = 'A'
    FREEZE = 'F'

    STATUS_CHOICES = (
        (ACTIVE, 'Active'),
        (FREEZE, 'Freeze'),
    )

    title = models.CharField(max_length=255, unique=True)
    description = models.TextField()
    status = models.CharField(max_length=1, choices=STATUS_CHOICES, default=ACTIVE)

    def __unicode__(self):
        return self.title


class Task(models.Model):

    URGENT = 2
    HIGH = 1
    MIDDLE = 0
    LOW = -1
    MAYBE = -2

    PRIORITY_CHOICES = (
        (URGENT, 'Urgent'),
        (HIGH, 'High'),
        (MIDDLE, 'Middle'),
        (LOW, 'Low'),
        (MAYBE, 'Maybe'),
    )

    (SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY) = range(7)

    DOW_CHOICES = (
        (SUNDAY, 'Sunday'),
        (MONDAY, 'Monday'),
        (TUESDAY, 'Tuesday'),
        (WEDNESDAY, 'Wednesday'),
        (THURSDAY, 'Thursday'),
        (FRIDAY, 'Friday'),
        (SATURDAY, 'Saturday'),
    )

    title = models.CharField(max_length=255)
    stream = models.ForeignKey(Stream)
    timestamp = models.DateTimeField(auto_now_add=True)
    priority = models.SmallIntegerField(choices=PRIORITY_CHOICES, default=0)
    planned_day = models.PositiveSmallIntegerField(blank=True, null=True)
    planned_month = models.PositiveSmallIntegerField(blank=True, null=True)
    planned_year = models.PositiveSmallIntegerField(blank=True, null=True)
    planned_dow = models.SmallIntegerField(choices=DOW_CHOICES, blank=True, null=True) # day of week

    def __unicode__(self):
        return self.title


class TaskContent(models.Model):

    task = models.ForeignKey(Task)
    content = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return 'Task#%d modification at %s' % (
                    self.task.pk,
                    datetime.datetime.strftime(self.timestamp, '%Y.%m.%d %H:%M'))
