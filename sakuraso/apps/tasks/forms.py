from django import forms

from .models import Stream


class StreamStatusForm(forms.Form):

    stream_id = forms.IntegerField(min_value=0)
    status = forms.CharField(max_length=1)

    def clean(self):
        data = self.cleaned_data
        # check status
        for sc in Stream.STATUS_CHOICES:
            if sc[0] == data.get('status'):
                break
        else:
            raise forms.ValidationError(u'Status with specified identifier not exist.')
        # check stream_id
        try:
            stream = Stream.objects.get(id=data.get('stream_id'))
        except Stream.DoesNotExist:
            raise forms.ValidationError(u'Stream with specified id does not exist.')
        data['stream'] = stream
        return data

    def save(self):
        stream = self.cleaned_data['stream']
        stream.status = self.cleaned_data['status']
        stream.save()
        return stream
