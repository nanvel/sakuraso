from django.shortcuts import render_to_response
from django.contrib.auth.decorators import login_required
from django.utils import simplejson
from django.http import HttpResponse, Http404

from sakuraso.apps.core.decorators import render_to

from .models import Stream
from .forms import StreamStatusForm


@login_required
@render_to('tasks/dashboard.html')
def index(request):
    return {}


@login_required
@render_to('tasks/streams.html')
def streams(request):
    streams = Stream.objects.all()
    return {'streams': streams}


@login_required
@render_to('tasks/stream.html')
def stream(request, stream_id):
    try:
        stream = Stream.objects.get(id=stream_id)
    except Stream.DoesNotExist:
        raise Http404
    return {'stream': stream}


@login_required
def stream_status_change(request):
    if not request.is_ajax():
        raise Http404
    form = StreamStatusForm(request.POST or None)
    data = {'success': False}
    if form.is_valid():
        stream = form.save()
        data = {'success': True, 'status': stream.get_status_display()}
    return HttpResponse(simplejson.dumps(data), mimetype='application/json')
