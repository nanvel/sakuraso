from django.test import TestCase
from django.core.urlresolvers import reverse
from django.conf import settings
from django.utils import simplejson

from ..models import Stream

from .factories import UserFactory, StreamFactory


class TasksViewsTestCase(TestCase):

    def test_views_auth(self):
        VIEWS_LOGIN_REQUIRED = ('tasks-dashboard', 'tasks-streams',
                                        'tasks-stream-status-change',)

        # test for unloggined user
        for view in VIEWS_LOGIN_REQUIRED:
            url = reverse(view)
            r = self.client.get(url)
            self.assertRedirects(r, '%s?next=%s' % (settings.LOGIN_URL, url))

    def test_views_smoke(self):
        VIEWS_TO_TEST = ('tasks-dashboard', 'tasks-streams',)

        # authenticated user
        user = UserFactory.create()
        self.client.login(username=user.username, password=user.username)
        for view in VIEWS_TO_TEST:
            r = self.client.get(reverse(view))
            self.assertEqual(r.status_code, 200)

    def test_stream_page(self):
        stream = StreamFactory.create()
        # try auth
        url = reverse('tasks-stream', kwargs={'stream_id': stream.id})
        r = self.client.get(url)
        self.assertRedirects(r, '%s?next=%s' % (settings.LOGIN_URL, url))
        # with login
        u = UserFactory.create()
        self.client.login(username=u.username, password=u.username)
        r = self.client.get(url)
        self.assertEqual(r.status_code, 200)

    def test_stream_status_change(self):
        u = UserFactory.create()
        self.client.login(username=u.username, password=u.username)
        stream = StreamFactory.create()
        self.assertEqual(stream.status, Stream.ACTIVE)
        r = self.client.post(
                        reverse('tasks-stream-status-change'),
                        {'stream_id': stream.id, 'status': 'F'},
                        HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(r.status_code, 200)
        data = simplejson.loads(r.content)
        self.assertTrue(data['success'])
        self.assertEqual(data['status'], 'Freeze')
        stream = Stream.objects.get(id=stream.id)
        self.assertEqual(stream.status, Stream.FREEZE)
