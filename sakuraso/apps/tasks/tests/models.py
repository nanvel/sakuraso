import datetime

from django.test import TestCase

from ..models import Stream, Task, TaskContent

from .factories import StreamFactory, TaskFactory


class TasksModelsTestCase(TestCase):

    def test_stream(self):
        title = 'Stream title'
        stream = Stream(title=title, description='Description')
        stream.save()
        self.assertEqual(Stream.objects.count(), 1)
        self.assertEqual(str(stream), title)

    def test_task(self):
        title = 'Task title'
        stream = StreamFactory.create()
        task = Task(stream=stream, title=title)
        task.save()
        self.assertEqual(Task.objects.count(), 1)
        self.assertEqual(str(task), title)

    def test_taskcontent(self):
        task = TaskFactory.create()
        task_content = TaskContent(content='Content', task=task)
        task_content.save()
        self.assertEqual(TaskContent.objects.count(), 1)
        task_content.timestamp = datetime.datetime(2013, 04, 17, 22, 52)
        self.assertEqual(
                str(task_content),
                'Task#%d modification at 2013.04.17 22:52' % task.pk)
