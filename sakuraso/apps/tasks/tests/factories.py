from django.contrib.auth.models import User

from ..models import Stream, Task, TaskContent


class StreamFactory:

    counter = 0

    @classmethod
    def create(self):
        self.counter += 1
        title = 'Task %d' % self.counter
        description = 'Description text'
        return Stream.objects.create(title=title, description=description)


class TaskFactory:

    counter = 0

    @classmethod
    def create(self, stream=None):
        self.counter += 1
        title = 'Task %d' % self.counter
        return Task.objects.create(
                    title=title,
                    stream=stream or StreamFactory.create())


class TaskContentFactory:

    counter = 0

    @classmethod
    def create(self, task=None):
        self.counter += 1
        content = 'Task content %d' % self.counter
        return TaskContent.objects.create(
                    content=content,
                    task=task or TaskFactory.create())


class UserFactory:

    counter = 0

    @classmethod
    def create(self):
        self.counter += 1
        username = 'user%d' % self.counter
        return User.objects.create_user(
                    username=username, email='%s@mail.com' % username,
                    password=username)
