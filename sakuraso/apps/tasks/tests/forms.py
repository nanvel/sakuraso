from django.test import TestCase

from ..forms import StreamStatusForm
from ..models import Stream

from .factories import StreamFactory


class TasksFormsTestCase(TestCase):

    def test_stream_status_form(self):

        stream = StreamFactory.create()

        test_data = [
            {
                'stream_id': stream.id,
                'status': 'F',
                'is_valid': True,
            },
            { # id as string
                'stream_id': str(stream.id),
                'status': 'F',
                'is_valid': True,
            },
            { # id not exists
                'stream_id': stream.id + 1000,
                'status': 'F',
                'is_valid': False,
            },
            { # bad status
                'stream_id': stream.id,
                'status': 'AX',
                'is_valid': False,
            },
            { # no status
                'stream_id': stream.id,
                'is_valid': False,
            },
            { # no id
                'is_valid': False,
            },
        ]

        for data in test_data:
            form = StreamStatusForm(data)
            self.assertEqual(form.is_valid(), data['is_valid'])

        form = StreamStatusForm(test_data[0])
        form.is_valid()
        self.assertEqual(form.cleaned_data['stream'].id, stream.id)
        self.assertEqual(stream.status, Stream.ACTIVE)
        form.save()
        stream = Stream.objects.get(id=stream.id)
        self.assertEqual(stream.status, Stream.FREEZE)
