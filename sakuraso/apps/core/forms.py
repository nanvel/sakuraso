from django import forms
from django.contrib.auth.forms import AuthenticationForm


class SakurasoLoginForm(AuthenticationForm):
    """
    Adds placeholders to login form
    """
    username = forms.CharField(
                max_length=254,
                widget=forms.TextInput(attrs={'placeholder': 'username'}))
    password = forms.CharField(
                label="Password",
                widget=forms.PasswordInput(attrs={'placeholder': 'password'}))
