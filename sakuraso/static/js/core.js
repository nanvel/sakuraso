jQuery(function ($, undefined) {
    var sakuraso = {};
    if (this.sakuraso !== undefined) {
        sakuraso = this.sakuraso;
    } else {
        this.sakuraso = sakuraso;
    }
    sakuraso.core = {
        init:function () {
        },
        cacheElements:function() {
        },
        showNotification:function(notification) {
            $('.js-notifications').html(notification).show();
        },
    }
    sakuraso.core.init();
});


