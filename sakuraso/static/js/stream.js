jQuery(function ($, undefined) {
    var sakuraso = {};
    if (this.sakuraso !== undefined) {
        sakuraso = this.sakuraso;
    } else {
        this.sakuraso = sakuraso;
    }
    sakuraso.stream = {
        init:function () {
            sakuraso.stream.cacheElements();
            sakuraso.stream.initStatusChange();
        },
        cacheElements:function() {
            sakuraso.stream.$changeStatusSelect = $('.js-stream-status');
            sakuraso.stream.id = $('.js-stream-title').data('id');
        },
        initStatusChange:function() {
            sakuraso.stream.$changeStatusSelect.on('change', function() {
                var status = $(this).val();
                $.ajax({
                    url: sakuraso.vars.streamStatusChangeURL,
                    dataType: "json",
                    data: {
                        stream_id: sakuraso.stream.id,
                        status: status,
                        csrfmiddlewaretoken: sakuraso.vars.csrfToken},
                    type: 'POST',
                    success: function (data) {
                        var success = data['success'];
                        if(success) {
                            sakuraso.core.showNotification('Stream status changed to "' + data['status'] + '".');
                        } else {
                            sakuraso.core.showNotification('An error occurred while changing stream status.');
                        }
                    },
                    error: function(){
                        sakuraso.core.showNotification('An error occurred while changing stream status.');
                    },
                });
            });
        },
    }
    sakuraso.stream.init();
});

