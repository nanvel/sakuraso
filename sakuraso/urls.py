from django.conf.urls import patterns, include, url

from django.contrib import admin

from sakuraso.apps.core.forms import SakurasoLoginForm


admin.autodiscover()


urlpatterns = patterns('',
    url(r'^login/$', 'django.contrib.auth.views.login', {
                'template_name': 'login.html',
                'authentication_form': SakurasoLoginForm}, name='login'),
    url(r'^logout/$', 'django.contrib.auth.views.logout', {'next_page': '/'}, name='logout'),

    url(r'^admin/', include(admin.site.urls)),

    url(r'', include('sakuraso.apps.tasks.urls')),
)
